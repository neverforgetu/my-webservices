from django.db import models

# Create your models here.
class LogWebservice(models.Model):
    id = models.AutoField(max_length=10, primary_key=True)
    title = models.CharField(max_length=50)
    date = models.CharField(max_length=50)
    start = models.CharField(max_length=50)
    finish = models.CharField(max_length=50)
    timestamp = models.CharField(max_length=50)
    description = models.TextField(max_length=200)