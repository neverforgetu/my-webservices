from rest_framework import serializers
from .models import LogWebservice

class LogWebserviceSerializer(serializers.ModelSerializer):

    class Meta:
        model = LogWebservice
        fields = ['id', 'title', 'date', 'start', 'finish', 'timestamp', 'description']
