from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from log_webservice import views

urlpatterns = [
    path('log_webservice/', views.LogWebserviceList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
