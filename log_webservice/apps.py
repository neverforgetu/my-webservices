from django.apps import AppConfig


class LogWebserviceConfig(AppConfig):
    name = 'log_webservice'
