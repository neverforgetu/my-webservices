from .models import LogWebservice
from .serializers import LogWebserviceSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

# Create your views here.
class LogWebserviceList(APIView):
    def get(self, request, format=None):
        activities = LogWebservice.objects.all()
        serializer = LogWebserviceSerializer(activities, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = LogWebserviceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)